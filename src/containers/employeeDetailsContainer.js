import EmployeeDetails from "../components/employeeDetails";
import { connect } from "react-redux";
import { withRouter } from "react-router";

function mapDispatchToProps(dispatch, props) {
  return {};
}

function mapStateToProps(state) {
  return {
    employeeDetails: state.employee.employeeDetails
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(EmployeeDetails)
);
