import EmployeeList from "../components/employeeList";
import { connect } from "react-redux";
import { getEmployeeList, getEmployeeDetails, deleteEmployee } from "../actions/employeeAction";
import { DELETE_EMPLOYEE, EDIT_EMPLOYEE_DETAIL_ROW, UPDATE_EMPLOYEE_NAME, UPDATE_EMPLOYEE_AGE, UPDATE_EMPLOYEE_SALARY} from "../const";
import { withRouter } from "react-router";


function mapDispatchToProps(dispatch, props) {
    return {
        getEmployeeList(){
            dispatch(getEmployeeList())
        },
        deleteEmployee(employee){
            dispatch(deleteEmployee(employee.id)).then(()=>{
                dispatch({type: DELETE_EMPLOYEE, payload: employee.id})
            })
        },
        viewEmployee(employee){
            dispatch(getEmployeeDetails(employee.id)).then(() => 
            {
                props.history.push("/details")
            })
        },
        editEmployee(employee){
            dispatch({ type: EDIT_EMPLOYEE_DETAIL_ROW, payload: employee.id })
        },
        handleChangeName(value){
            dispatch({ type: UPDATE_EMPLOYEE_NAME, payload: value })
        },
        handleChangeAge(value){
            dispatch({ type: UPDATE_EMPLOYEE_AGE, payload: value })
        },
        handleChangeSalary(value){
            dispatch({ type: UPDATE_EMPLOYEE_SALARY, payload: value })
        }
    }
}

function mapStateToProps(state) {
    console.log("employee list", state.employee.employeeList)
    return {
        employeeList: state.employee.employeeList
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EmployeeList));
