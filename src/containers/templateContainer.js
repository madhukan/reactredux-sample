import Template from "../components/template";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { getSampleData } from "../actions/templateActions";

function mapDispatchToProps(dispatch, props) {
  return {
      getSampleData(){
          dispatch(getSampleData())
      }
  };
}

function mapStateToProps(state) {
  return {};
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Template)
);
