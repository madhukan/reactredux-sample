import EmployeeCreate from "../components/employeeCreate";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { ON_CHANGE_NAME, ON_CHANGE_AGE, ON_CHANGE_SALARY } from "../const";
import { handleSubmit } from "../actions/employeeAction";

function mapDispatchToProps(dispatch, props) {
  return {
    handleChangeName(event){
      dispatch({ type: ON_CHANGE_NAME, payload: event.target.value});
    },
    handleChangeAge(event){
      dispatch({ type: ON_CHANGE_AGE, payload: event.target.value});
    },
    handleChangeSalary(event){
      dispatch({ type: ON_CHANGE_SALARY, payload: event.target.value});
    },
    handleSubmit(){
      dispatch(handleSubmit()).then(() =>{
        props.history.push("/")
      });
    }
  };
}

function mapStateToProps(state) {
  return {
  
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(EmployeeCreate)
);
