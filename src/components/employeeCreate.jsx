import React, { Component } from "react";

class EmployeeCreate extends Component {
  render() {
    return (
      <div>
        <div className="title">Create</div>
        <div className="inputField">
          <label>Employee Name</label>
          <br />
          <input
            name="name"
            type="text"
            placeholder="Employee Name"
            onChange={event => this.props.handleChangeName(event)}
          />
        </div>
        <div className="inputField">
          <label>Age</label>
          <br />
          <input
            name="age"
            type="text"
            placeholder="Age"
            onChange={event => this.props.handleChangeAge(event)}
          />
        </div>
        <div className="inputField">
          <label>Salary</label>
          <br />
          <input
            name="salary"
            type="text"
            placeholder="Salary"
            onChange={event => this.props.handleChangeSalary(event)}
          />
        </div>
        <div>
          <button className="submit" onClick={() => this.props.handleSubmit()}>
            Submit
          </button>
        </div>
      </div>
    );
  }
}

export default EmployeeCreate;
