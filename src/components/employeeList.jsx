import React, { Component } from "react";
import { Link } from 'react-router-dom';

class EmployeeList extends Component {
  componentDidMount() {
    this.props.getEmployeeList();
  }
  render() {
    return (
      <div>
        <div className="title">Employee List
        <span>
          <Link to="/create">
          <button className="create">Create</button>
          </Link>
          </span></div>
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Age</th>
              <th>Salary</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.props.employeeList.map((employee, index) => {
              return (
                <tr key={index} className="employeeList">
                  <td>{employee.id}</td>
                  {employee.isEditable ? <td><input type="text" name="id" value={employee.employee_name} onChange={(event) => this.props.handleChangeName(event.target.value)}/></td>:<td>{employee.employee_name}</td>}
                  {employee.isEditable ? <td><input type="text" name="id" value={employee.employee_age} onChange={(event) => this.props.handleChangeAge(event.target.value)}/></td>:<td>{employee.employee_age}</td>}
                  {employee.isEditable ? <td><input type="text" name="id" value={employee.employee_salary} onChange={(event) => this.props.handleChangeSalary(event.target.value)}/></td>:<td>{employee.employee_salary}</td>}
                  <td>
                    <button className="view" onClick={() => this.props.viewEmployee(employee)}>
                      View
                    </button>
                    <button className="edit" onClick={() => this.props.editEmployee(employee)}>
                      Edit
                    </button>
                    <button className="delete" onClick={() => this.props.deleteEmployee(employee)}>
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default EmployeeList;
