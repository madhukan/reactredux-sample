import React, { Component } from "react";

class EmployeeDetails extends Component {
  
  render() {
      const {employeeDetails} = this.props
    return (
      <div>
          <div className="title">Employee Details</div>
          {employeeDetails && <table>
              <tbody>
              <tr>
                  <td>ID: </td>
                  <td>{employeeDetails.id}</td>
              </tr>
              <tr>
                  <td>Name: </td>
                  <td>{employeeDetails.employee_name}</td>
              </tr>
              <tr>
                  <td>Age: </td>
                  <td>{employeeDetails.employee_age}</td>
              </tr>
              <tr>
                  <td>Salary: </td>
                  <td>{employeeDetails.employee_salary}</td>
              </tr>
              </tbody>
          </table>}
      </div>
    );
  }
}

export default EmployeeDetails;
