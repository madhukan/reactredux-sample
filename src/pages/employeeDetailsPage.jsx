import React, { Component } from "react";
import EmployeeDetailsContainer from "../containers/employeeDetailsContainer";

class EmployeeDetailsPage extends Component {
  render() {
    return <EmployeeDetailsContainer />;
  }
}

export default EmployeeDetailsPage;
