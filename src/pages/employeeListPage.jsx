import React, { Component } from "react";
import EmployeeListContainer from "../containers/employeeListContainer";

class EmployeeListPage extends Component {
  render() {
    return <EmployeeListContainer />;
  }
}

export default EmployeeListPage;
