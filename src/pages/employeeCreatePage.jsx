import React, { Component } from "react";
import EmployeeCreateContainer from "../containers/employeeCreateContainer";

class EmployeeCreatePage extends Component {
  render() {
    return <EmployeeCreateContainer />;
  }
}

export default EmployeeCreatePage;
