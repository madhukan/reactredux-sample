import React, { Component } from "react";
import TemplateContainer from "../containers/templateContainer";

class TemplatePage extends Component {
  render() {
    return <TemplateContainer />;
  }
}

export default TemplatePage;
