import axios from "axios";
import { GET_EMPLOYEE_LIST, GET_EMPLOYEE_DETAILS, SAVE_EMPLOYEE_DETAILS, DELETE_EMPLOYEE_DETAILS } from "../const";
import store from "../store";

export function getEmployeeList(){
    return{
        type: GET_EMPLOYEE_LIST,
        payload:axios.get('http://dummy.restapiexample.com/api/v1/employees')
    }

}

export function getEmployeeDetails(employeeID){
    return{
        type: GET_EMPLOYEE_DETAILS,
        payload:axios.get('http://dummy.restapiexample.com/api/v1/employee/' + employeeID)
    }
}

export function handleSubmit(){
    let postObject={
        name: store.getState().employee.name,
        age: store.getState().employee.age,
        salary: store.getState().employee.salary
    }
    return{
        type: SAVE_EMPLOYEE_DETAILS,
        payload:axios.post('http://dummy.restapiexample.com/api/v1/create', postObject)
    }
}

export function deleteEmployee(employeeID){
    return{
        type: DELETE_EMPLOYEE_DETAILS,
        payload:axios.delete('http://dummy.restapiexample.com/api/v1/delete/' + employeeID)
    }
}