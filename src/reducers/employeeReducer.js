import {
  GET_EMPLOYEE_LIST_FULFILLED,
  GET_EMPLOYEE_LIST_REJECTED,
  GET_EMPLOYEE_LIST_PENDING,
  GET_EMPLOYEE_DETAILS_FULFILLED,
  GET_EMPLOYEE_DETAILS_REJECTED,
  GET_EMPLOYEE_DETAILS_PENDING,
  DELETE_EMPLOYEE,
  ON_CHANGE_NAME,
  ON_CHANGE_AGE,
  ON_CHANGE_SALARY,
  SAVE_EMPLOYEE_DETAILS_PENDING,
  SAVE_EMPLOYEE_DETAILS_FULFILLED,
  SAVE_EMPLOYEE_DETAILS_REJECTED,
  DELETE_EMPLOYEE_DETAILS_PENDING,
  DELETE_EMPLOYEE_DETAILS_FULFILLED,
  DELETE_EMPLOYEE_DETAILS_REJECTED,
  EDIT_EMPLOYEE_DETAIL_ROW,
  UPDATE_EMPLOYEE_NAME,
} from "../const";

const initialState = {
  employeeList: [],
  name: "",
  age: "",
  salary: "",
  successCreateMessage: "",
  successDeleteMessage: "",
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_EMPLOYEE_LIST_PENDING:
      return { ...state };
    case GET_EMPLOYEE_LIST_FULFILLED:
      action.payload &&
        action.payload.data &&
        action.payload.data.data.map((item) => {
          item.isEditable = false;
        });
      return { ...state, employeeList: action.payload.data.data };
    case GET_EMPLOYEE_LIST_REJECTED:
      return { ...state };
    case GET_EMPLOYEE_DETAILS_PENDING:
      return { ...state };
    case GET_EMPLOYEE_DETAILS_FULFILLED:
      return { ...state, employeeDetails: action.payload.data };
    case GET_EMPLOYEE_DETAILS_REJECTED:
      return { ...state };
    case DELETE_EMPLOYEE:
      return {
        ...state,
        employeeList: [
          ...state.employeeList.filter(
            (employee) => employee.id !== action.payload
          ),
        ],
        successDeleteMessage: "Deleted Successfully",
      };
    case ON_CHANGE_NAME:
      return { ...state, name: action.payload };
    case ON_CHANGE_AGE:
      return { ...state, age: action.payload };
    case ON_CHANGE_SALARY:
      return { ...state, salary: action.payload };
    case SAVE_EMPLOYEE_DETAILS_PENDING:
      return { ...state };
    case SAVE_EMPLOYEE_DETAILS_FULFILLED:
      return { ...state, successCreateMessage: action.payload.data };
    case SAVE_EMPLOYEE_DETAILS_REJECTED:
      return { ...state };
    case DELETE_EMPLOYEE_DETAILS_PENDING:
      return { ...state };
    case DELETE_EMPLOYEE_DETAILS_FULFILLED:
      return {
        ...state,
        successDeleteMessage: action.payload.data.success.text,
      };
    case DELETE_EMPLOYEE_DETAILS_REJECTED:
      return { ...state };
    case EDIT_EMPLOYEE_DETAIL_ROW:
      const employeeList = state.employeeList.map((item) => {
        if (item.id === action.payload) {
          return { ...item, ...(item.isEditable = true) };
        }
        return item;
      });
      return { ...state, employeeList };
    case UPDATE_EMPLOYEE_NAME:
      return {
        ...state,
        employeeList: [
          ...state.employeeList.map((item) => {
            if (item.id === action.payload) {
              return { ...item, ...(item.employee_name = action.payload) };
            }
            return item;
          }),
        ],
      };
    default:
      return state;
  }
}
