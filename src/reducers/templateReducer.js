import {} from "../const";

const initialState = {
  sample: ""
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case "SAMPLE_ACTION_TYPE_FULFILLED":
      return { ...state, sample: action.payload.data };
    case "SAMPLE_ACTION_TYPE_PENDING":
      return { ...state, sample: action.payload.data };
    case "SAMPLE_ACTION_TYPE_REJECTED":
      return { ...state, sample: action.payload.data };
    default:
      return state;
  }
}
