import { combineReducers } from 'redux';

import employee from './employeeReducer';
//import template from './templateReducer';

export default combineReducers({
    employee,
    //template
})