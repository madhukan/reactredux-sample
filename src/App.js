import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import EmployeeListPage from "./pages/employeeListPage";
import EmployeeDetailsPage from "./pages/employeeDetailsPage";
import EmployeeCreatePage from "./pages/employeeCreatePage";
import TemplatePage from "./pages/templatePage";
import Hooks from "./components/Hooks";
import Sample from "./components/Normal";
import "./assets/main.css";

export class App extends Component {
  render() {
    return (
      <Router>
        <Route exact path="/" component={EmployeeListPage} />
        <Route exact path="/details" component={EmployeeDetailsPage} />
        <Route exact path="/create" component={EmployeeCreatePage} />
        <Route exact path="/template" component={TemplatePage} />
        <Route exact path="/hooks" component={Hooks} />
        <Route exact path="/sample" component={Sample} />
      </Router>
    );
  }
}

export default App;
